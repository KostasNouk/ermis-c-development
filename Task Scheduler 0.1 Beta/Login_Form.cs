﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Oracle.DataAccess.Client;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace Task_Scheduler_0._1_Beta
{
    public partial class Login_Form : Form
    {
        public Login_Form()
        {
            InitializeComponent();
        }
        private void cancelbn_Click(object sender, EventArgs e)
        {
            Close();
        }
        private void loginbn_Click(object sender, EventArgs e)
        {
            Regex re = new Regex("^[0-9a-zA-Z ]+$");
            if (!re.IsMatch(usrpwdtxtbx.Text) || !re.IsMatch(usrnmtxtbx.Text))
            {
                MessageBox.Show("Only Alphanumeric characters are allowed in the Login area.");
            }
            else
            {
                Variables.username = usrnmtxtbx.Text;
                UTF8Encoding ue = new UTF8Encoding();
                byte[] hashValue;
                byte[] message = ue.GetBytes(usrpwdtxtbx.Text);
                SHA256Managed hashString = new SHA256Managed();
                string hex = "";
                hashValue = hashString.ComputeHash(message);
                foreach (byte x in hashValue)
                {
                    hex += String.Format("{0:x2}", x);
                }
                string encrypted_pwd = hex;

                try
                {
                    if (String.IsNullOrWhiteSpace(usrnmtxtbx.Text) || String.IsNullOrWhiteSpace(usrpwdtxtbx.Text))
                    {
                        MessageBox.Show("Username and Password are required fields. Enter Username and Password to continue.");
                        return;
                    }

                    string queryString = "SELECT * FROM USERS WHERE USERS.USERNAME='" + this.usrnmtxtbx.Text + "'AND USERS.PASSWORD ='" + encrypted_pwd + "'";
                    OracleConnection conn = new OracleConnection("Data Source=QWE;Persist Security Info=True;User ID=QWE;Password=QWE;");
                    conn.Open();
                    OracleCommand sqlcon = new OracleCommand(queryString, conn);
                    sqlcon.Parameters.Add(new OracleParameter("USERNAME", usrnmtxtbx.Text));
                    sqlcon.Parameters.Add(new OracleParameter("PASSWORD", encrypted_pwd));
                    OracleDataAdapter koumpwse_to = new OracleDataAdapter(sqlcon);
                    DataSet ds = new DataSet();
                    koumpwse_to.Fill(ds);
                    Variables.usrid = usrnmtxtbx.Text;

                    /*------------------GET GROUPID AND ACCESSLEVEL---------------------------------------------------------------------------------------------*/
                    OracleCommand parsedept = new OracleCommand();
                    parsedept.Connection = conn;
                    string deptparser = "SELECT * FROM USERS WHERE TRIM(USERNAME) = TRIM('" + Variables.usrid + "')";
                    parsedept.CommandText = deptparser;
                    string mydept = parsedept.ExecuteScalar().ToString();
                    OracleDataReader reader = parsedept.ExecuteReader();

                    while (reader.Read())
                    {
                        string meindept = Convert.ToString(reader[4]);
                        string meinacclvl = Convert.ToString(reader[5]);
                        string tempid = Convert.ToString(reader[0]);
                        Variables.usrdep = meindept;
                        Variables.parseacclevel = meinacclvl;
                        Variables.user_id = tempid;
                    }

                    //Travaw to groupid kai to accesslevel tou atomou pou syndeetai gia na periorisw ta dikaiwmata me vash ton arithmo id.
                  
                    int i = ds.Tables[0].Rows.Count;
                    if ((i == 1) && (Variables.parseacclevel == "1"))
                    {
                        this.Hide();
                        Task_Scheduler frm = new Task_Scheduler();
                        frm.Show();
                        ds.Clear();
                        conn.Close(); 
                    }
                    else if ((i == 1) && (Variables.parseacclevel == "2"))
                    {
                        this.Hide();
                        Scheduler sch = new Scheduler();
                        sch.Show();
                        ds.Clear();
                        conn.Close();
                    }
                    else if ((i == 1) && (Variables.parseacclevel == "3"))
                    {
                        //MessageBox.Show(Variables.usrdep);
                        this.Hide();
                        ERMIS_TS_0._5_Beta.sumainform su = new ERMIS_TS_0._5_Beta.sumainform();
                        su.Show();
                        //new form for superusers. The form should allow a) insert tasks b)check tasks c) check scheduler 
                        //new form show 
                        ds.Clear();
                        conn.Close();
                    }
                    else
                    {
                        MessageBox.Show("Not Registered User or Invalid Name/Password");
                        usrnmtxtbx.Text = "";
                        usrpwdtxtbx.Text = "";
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
#region globalvariables
    class Variables
    {
        public static string username;
        public static string usrid;
        public static string usrdep;
        public static int flag = 0;
        public static int accesslevel = 0;
        public static string departmentstring = "";
        public static string parseacclevel;
        //for parsing at departmentrights
        public static string createnewusername = "";
        public static string encrypted_pwd = "";
        public static string newusernameboxtext = "";
        public static string newnameboxtext = "";
        public static string global_pwd = "";
        public static string user_id = "";
    }
#endregion
}
