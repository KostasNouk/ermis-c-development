﻿using System;
using System.Data;
using System.Windows.Forms;
using Oracle.DataAccess.Client;
using System.Text.RegularExpressions;

namespace Task_Scheduler_0._1_Beta
{
    public partial class Add_New_Task : Form
    {      
        public Add_New_Task()
        {
            InitializeComponent();
            Controls.AddRange(new Control[] {startdate});      
        }

        DateTime tempdatestart = new DateTime();
        DateTime parsedatetime = new DateTime();
        int intervalval;
        
        private void button1_Click(object sender, EventArgs e)
        {
            Regex re = new Regex(@"^[0-9a-zA-Z ; @ < ) > . ( \s ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩαβγδεζηθικλμνξοπρστυφχψωάέήίόύς]+$");
            if (!re.IsMatch(newtasknamebox.Text) || !re.IsMatch(newtasknotesbox.Text))
            {
                MessageBox.Show("Invalid Character Input!");
            }
            else
            {
                #region deadlinechoice
                startdate.Format = DateTimePickerFormat.Custom;
                startdate.CustomFormat = "dd/MM/yyyy HH:mm:ss";
                tempdatestart = startdate.Value;
                //Παίρνω το επιθυμητό groupid για να το χρησιμοποιήσω στο Insert του νεόυ Task. Έτσι όταν χρειαστεί να αναθέσω task σε department γίνεται εφικτό. 
                OracleConnection conn = new OracleConnection("Data Source=QWE;Persist Security Info=True;User ID=QWE;Password=QWE;");
                conn.Open();
                OracleCommand parsechoice = new OracleCommand();
                parsechoice.Connection = conn;
                string choiceparser = "SELECT GROUPID, DEPARTMENT FROM GROUPS";
                parsechoice.CommandText = choiceparser;
                string dbdata = parsechoice.ExecuteScalar().ToString();
                OracleDataReader reader = parsechoice.ExecuteReader();

                string selected = this.departmentbox.GetItemText(this.departmentbox.SelectedItem);
                //MessageBox.Show(selected);

                while (reader.Read())
                {
                    if (selected == Convert.ToString(reader[1]))
                    {
                        Variables.departmentstring = Convert.ToString(reader[0]);
                       // MessageBox.Show(Variables.departmentstring);
                    }                                
                }
                conn.Close();

                if (choiceinterval.Text.Equals("Every Day"))
                {
                    parsedatetime = startdate.Value.AddHours(12);
                    intervalval = 1;
                    executeinsert();
                }
                else if (choiceinterval.Text.Equals("Every 2 Days"))
                {
                    parsedatetime = startdate.Value.AddDays(2);
                    intervalval = 2;
                    executeinsert();
                }
                else if (choiceinterval.Text.Equals("Every 3 Days"))
                {
                    parsedatetime = startdate.Value.AddDays(3);
                    intervalval = 3;
                    executeinsert();
                }
                else if (choiceinterval.Text.Equals("Every 4 Days"))
                {
                    parsedatetime = startdate.Value.AddDays(4);
                    intervalval = 4;
                    executeinsert();
                }
                else if (choiceinterval.Text.Equals("Every 5 Days"))
                {
                    parsedatetime = startdate.Value.AddDays(5);
                    intervalval = 5;
                    executeinsert();
                }
                else if (choiceinterval.Text.Equals("Every Week"))
                {
                    parsedatetime = startdate.Value.AddDays(7);
                    intervalval = 7;
                    executeinsert();
                }
                else if (choiceinterval.Text.Equals("Every 2 Weeks"))
                {
                    parsedatetime = startdate.Value.AddDays(14);
                    intervalval = 14;
                    executeinsert();
                }
                else if (choiceinterval.Text.Equals("Every Month"))
                {
                    parsedatetime = startdate.Value.AddMonths(1);
                    intervalval = 30;
                    executeinsert();
                }
                else if (choiceinterval.Text.Equals("Every Minute (Tests Only)"))
                {
                    parsedatetime = startdate.Value.AddMinutes(1);
                    intervalval = 60;
                    executeinsert();
                }
                else
                {
                    MessageBox.Show("You must choose a deadline and a department to complete the process!");
                }
                #endregion
                clearvars();
            }
        }

#region executeinsert
        public void executeinsert()
        {
                    string sysinfo = SystemInformation.ComputerName;
                    try
                    {
                        OracleConnection conn = new OracleConnection("Data Source=QWE;Persist Security Info=True;User ID=QWE;Password=QWE;");
                        conn.Open();
                        OracleCommand parseid = new OracleCommand();
                        parseid.Connection = conn;
                        string idparser = "SELECT USERID, USERNAME FROM USERS WHERE TRIM(USERNAME) = TRIM('" + Variables.usrid + "')";
                        parseid.CommandText = idparser;
                        string myid = parseid.ExecuteScalar().ToString();
                        OracleDataReader reader = parseid.ExecuteReader();
                        string tempid = "";
                        string tempname = "";

                        while (reader.Read())
                        {
                            string meinid = Convert.ToString(reader[0]);
                            string myname = Convert.ToString(reader[1]);
                            tempid = meinid;
                            tempname = myname;
                        }

                        OracleCommand sqlcom = new OracleCommand();
                        sqlcom.Connection = conn;
                        string temp = "INSERT INTO TASKS (TASKID,TASKJOB,INSERTDATE,TASKNOTE,STARTDATE, DEVICE, USERID, USERNAME, INTERVAL, GROUPID) VALUES((SELECT NVL(MAX(TASKID),0)+1 FROM TASKS),'" + this.newtasknamebox.Text + "',SYSDATE,'" + this.newtasknotesbox.Text + "',TO_DATE('"+tempdatestart.ToString("dd/MM/yyyy HH:mm:ss")+"', 'dd/mm/yyyy HH24:MI:SS'), '" + sysinfo + "' ,'" + Convert.ToInt32(tempid) + "','" +tempname+ "', '"+intervalval+"', '"+Variables.departmentstring+"')";
                        sqlcom.CommandText = temp;
                        //MessageBox.Show(temp);
                        OracleCommand deadlinecom = new OracleCommand();
                        deadlinecom.Connection = conn;
                        string tempdata = "INSERT INTO SCHEDULERTASK(SCHEDULERTASKID, TASKID, INSERTDATE, TASKJOB, TASKNOTE, DEADLINE) SELECT(SELECT NVL(MAX(SCHEDULERTASKID), 0) FROM SCHEDULERTASK) + ROWNUM, TASKS.TASKID, TASKS.INSERTDATE, TASKS.TASKJOB, TASKS.TASKNOTE,TO_DATE('"+parsedatetime.ToString("dd/MM/yyyy HH:mm:ss")+"', 'dd/mm/yyyy HH24:MI:SS') FROM TASKS WHERE TASKS.TASKJOB = '" + this.newtasknamebox.Text + "'";
                        //MessageBox.Show(tempdata);
                        deadlinecom.CommandText = tempdata;
                        //Environment.Exit(0);
                        parseid.ExecuteNonQuery();
                        sqlcom.ExecuteNonQuery();
                        deadlinecom.ExecuteNonQuery();
                        conn.Close();
                        MessageBox.Show("Done!");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
        }
#endregion
#region clearvals
        public void clearvars()
        {
            Variables.accesslevel = 0;
            newtasknamebox.Text = "";
            newtasknotesbox.Text = "";
        }
        #endregion
#region populatecombobox
        private void load_data_to_departmentbox()
       {
         OracleConnection conn = new OracleConnection("Data Source=QWE;Persist Security Info=True;User ID=QWE;Password=QWE;");
         conn.Open();                
         OracleDataAdapter ord = new OracleDataAdapter("SELECT GROUPS.DEPARTMENT FROM GROUPS;", conn);
         DataTable dt = new DataTable();           
         ord.Fill(dt);           
         for (int i = 0; i < dt.Rows.Count; ++i)           
         {
             departmentbox.Items.Add(dt.Rows[i]["DEPARTMENT"]);
         }
         conn.Close();     
       }

        private void Add_New_Task_Load(object sender, EventArgs e)
        {
            load_data_to_departmentbox();
        }
#endregion
    }
}
