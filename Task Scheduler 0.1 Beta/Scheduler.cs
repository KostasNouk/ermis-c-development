﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace Task_Scheduler_0._1_Beta
{
    public partial class Scheduler : Form
    {
        private int col = -1;
        private int row = -1;

        public Scheduler()
        {
            InitializeComponent();
        }

        public void RowsColor()
        {
            for (int i = 0; i < schedulegrid.Rows.Count; i++)
            {
                string val = schedulegrid.Rows[i].Cells[6].Value.ToString();
                if (val == "1")
                {
                    schedulegrid.Rows[i].DefaultCellStyle.BackColor = Color.YellowGreen;
                }
            }
        }
        private void taskfinishbn_Click(object sender, EventArgs e)
        {
            try
            {
                OracleConnection conn = new OracleConnection("Data Source=QWE;Persist Security Info=True;User ID=QWE;Password=QWE;");
                OracleCommand sqlcom = new OracleCommand();
                sqlcom.Connection = conn;
                string loadselect = "SELECT * FROM SCHEDULERTASK WHERE EXISTS (SELECT * FROM TASKS WHERE TASKS.TASKID = SCHEDULERTASK.TASKID AND TASKS.ENABLED = 1 AND TASKS.GROUPID = '"+Variables.usrdep+"') ORDER BY SCHEDULERTASKID ASC";
                sqlcom.CommandText = loadselect;
                conn.Open();
                OracleDataAdapter odb = new OracleDataAdapter();
                odb.SelectCommand = sqlcom;
                DataTable dbdataset = new DataTable();
                odb.Fill(dbdataset);
                BindingSource bsource = new BindingSource();
                bsource.DataSource = dbdataset;
                schedulegrid.DataSource = bsource;
                odb.Update(dbdataset);
                conn.Close();
              //MessageBox.Show("Done!");      
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            RowsColor();
        }

        private void taskexitbn_Click(object sender, EventArgs e)
        {
            if ((Variables.parseacclevel == "2") && (MessageBox.Show("Are you sure you want to exit ERMIS TS?", "Exit", MessageBoxButtons.OKCancel) == DialogResult.OK))
            {
                Environment.Exit(0);
            }
            else 
            {
                this.Close();
            }           
        }

        private void schedulegrid_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            ContextMenuStrip mnu = new ContextMenuStrip();
            ToolStripMenuItem mnuok = new ToolStripMenuItem("Mark Task As Finished");
            mnuok.Click += new EventHandler(mnuok_Click);
            mnu.Items.AddRange(new ToolStripItem[] {mnuok});
            schedulegrid.ContextMenuStrip = mnu;
            if (e.Button == MouseButtons.Right)
            {
                row = e.RowIndex;
                col = e.ColumnIndex;
            }
        }

        private void mnuok_Click(object sender, EventArgs e)
        {
            string sysinfo = SystemInformation.ComputerName;
            OracleConnection conn = new OracleConnection("Data Source=QWE;Persist Security Info=True;User ID=QWE;Password=QWE;");
            OracleCommand sqlcom = new OracleCommand();
            sqlcom.Connection = conn;   
            foreach (DataGridViewRow row in schedulegrid.SelectedRows)
            {  
                string value = Convert.ToString(row.Cells[0].Value);
                try
                {
                    conn.Open();
                    string temp = "UPDATE SCHEDULERTASK SET ISOK = 1, ISOK_DATE = SYSDATE, USERNAME = '"+Variables.username+"', MACHINENAME = '"+sysinfo+"' WHERE SCHEDULERTASK.SCHEDULERTASKID = '"+Convert.ToInt32(value)+"'";
                    sqlcom.CommandText = temp;
                    sqlcom.ExecuteNonQuery();
                    conn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }               
            }
            MessageBox.Show("Done!");
            showscheduledtasksbn.PerformClick();
        }

        private void Scheduler_Load(object sender, EventArgs e)
        {
            if (Variables.parseacclevel == "2")
            {
                welcomelbl.Text = "Welcome, "+Variables.usrid+"! Here are your ongoing tasks.";
            }
        }

        private void Scheduler_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ((Variables.parseacclevel == "2") && (MessageBox.Show("Are you sure you want to exit ERMIS TS?", "Exit", MessageBoxButtons.OKCancel) == DialogResult.OK))
            {
                Environment.Exit(0);
            }
        }
    }
}
