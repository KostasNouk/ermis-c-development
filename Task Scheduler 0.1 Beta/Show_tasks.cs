﻿using System;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace Task_Scheduler_0._1_Beta
{
    public partial class Show_tasks : Form
    {
        private int col = -1;
        private int row = -1;

        public Show_tasks()
        {
            InitializeComponent();
        }
        private void activetasksbn_Click(object sender, EventArgs e)
        {
           Variables.flag = 1;
                try
                {
                    String loadselect = "";
                    OracleConnection conn = new OracleConnection("Data Source=QWE;Persist Security Info=True;User ID=QWE;Password=QWE;");
                    OracleCommand sqlcom = new OracleCommand();
                    sqlcom.Connection = conn;
                    if (Variables.parseacclevel != "3")
                    {
                        loadselect = "SELECT * FROM QWE.TASKS WHERE TASKS.ENABLED = 1 ORDER BY TASKID ASC";
                    }
                    else
                    {
                        loadselect = "SELECT TSK.TASKID, TSK.TASKJOB, TSK.INSERTDATE, TSK.TASKNOTE, TSK.STARTDATE, TSK.ENABLED, TSK.DEVICE, TSK.USERNAME, TSK.ENABLESYSDATE, TSK.DISABLESYSDATE, TSK.INTERVAL, TSK.GROUPID FROM TASKS TSK, USERRIGHTS USR, USERS U WHERE TSK.GROUPID = USR.GROUPID AND U.USERID = USR.USERID AND USR.USERID = '"+Variables.user_id+ "' AND TSK.ENABLED = 1 ORDER BY TASKID ASC";
                    }
                    sqlcom.CommandText = loadselect;
                    conn.Open();
                    OracleDataAdapter odb = new OracleDataAdapter();
                    odb.SelectCommand = sqlcom;
                    DataTable dbdataset = new DataTable();
                    odb.Fill(dbdataset);
                    BindingSource bsource = new BindingSource();
                    bsource.DataSource = dbdataset;
                    tasksgrid.DataSource = bsource;
                    odb.Update(dbdataset);
                    conn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            RowsColor();
        }

        private void taskdataloadbn_Click(object sender, EventArgs e)
        {
            Variables.flag = 2;
            try
            {
                String loadselect = "";
                OracleConnection conn = new OracleConnection("Data Source=QWE;Persist Security Info=True;User ID=QWE;Password=QWE;");
                OracleCommand sqlcom = new OracleCommand();
                sqlcom.Connection = conn;
                if (Variables.parseacclevel != "3")
                {
                    loadselect = "SELECT * FROM QWE.TASKS WHERE TASKS.ENABLED = 1 ORDER BY TASKID ASC";
                }
                else
                {
                    loadselect = "SELECT TSK.TASKID, TSK.TASKJOB, TSK.INSERTDATE, TSK.TASKNOTE, TSK.STARTDATE, TSK.ENABLED, TSK.DEVICE, TSK.USERNAME, TSK.ENABLESYSDATE, TSK.DISABLESYSDATE, TSK.INTERVAL, TSK.GROUPID FROM TASKS TSK, USERRIGHTS USR, USERS U WHERE TSK.GROUPID = USR.GROUPID AND U.USERID = USR.USERID AND USR.USERID = '" + Variables.user_id + "' ORDER BY TASKID ASC";
                }
                sqlcom.CommandText = loadselect;
                conn.Open();
                OracleDataAdapter odb = new OracleDataAdapter();
                odb.SelectCommand = sqlcom;
                DataTable dbdataset = new DataTable();
                odb.Fill(dbdataset);
                BindingSource bsource = new BindingSource();
                bsource.DataSource = dbdataset;
                tasksgrid.DataSource = bsource;
                odb.Update(dbdataset);
                conn.Close();     
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            RowsColor();
        }

        private void taskfinishbn_Click(object sender, EventArgs e)
        {
            Variables.flag = 3;
            try
            {
                String loadselect = "";
                OracleConnection conn = new OracleConnection("Data Source=QWE;Persist Security Info=True;User ID=QWE;Password=QWE;");
                OracleCommand sqlcom = new OracleCommand();
                sqlcom.Connection = conn;
                if (Variables.parseacclevel != "3")
                {
                    loadselect = "SELECT * FROM QWE.TASKS WHERE TASKS.ENABLED = 1 ORDER BY TASKID ASC";
                }
                else
                {
                    loadselect = "SELECT TSK.TASKID, TSK.TASKJOB, TSK.INSERTDATE, TSK.TASKNOTE, TSK.STARTDATE, TSK.ENABLED, TSK.DEVICE, TSK.USERNAME, TSK.ENABLESYSDATE, TSK.DISABLESYSDATE, TSK.INTERVAL, TSK.GROUPID FROM TASKS TSK, USERRIGHTS USR, USERS U WHERE TSK.GROUPID = USR.GROUPID AND U.USERID = USR.USERID AND USR.USERID = '" + Variables.user_id + "'AND TSK.ENABLED = 0 ORDER BY TASKID ASC";
                }
                sqlcom.CommandText = loadselect;
                conn.Open();
                OracleDataAdapter odb = new OracleDataAdapter();
                odb.SelectCommand = sqlcom;
                DataTable dbdataset = new DataTable();
                odb.Fill(dbdataset);
                BindingSource bsource = new BindingSource();
                bsource.DataSource = dbdataset;
                tasksgrid.DataSource = bsource;
                odb.Update(dbdataset);
                conn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            RowsColor();
        }
        private void taskexitbn_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        public void RowsColor()
        {
            for (int i = 0; i < tasksgrid.Rows.Count; i++)
            {
                    string val1 = tasksgrid.Rows[i].Cells[5].Value.ToString();
                    string val = tasksgrid.Rows[i].Cells[9].Value.ToString();
                    if (val == "1" && val1 != "0")
                    {
                        tasksgrid.Rows[i].DefaultCellStyle.BackColor = Color.Red;
                    }
                    else if (val == "2" && val1 != "0")
                    {
                    tasksgrid.Rows[i].DefaultCellStyle.BackColor = Color.Gold;
                    }
            }
        }

        private void tasksgrid_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            ContextMenuStrip mnu = new ContextMenuStrip();
            ToolStripMenuItem mnuena = new ToolStripMenuItem("Mark Task As Enabled");
            ToolStripMenuItem mnufin = new ToolStripMenuItem("Mark Task As Disabled");
            mnuena.Click += new EventHandler(mnuena_Click);
            mnufin.Click += new EventHandler(mnufin_Click);
            mnu.Items.AddRange(new ToolStripItem[] { mnuena, mnufin});
            tasksgrid.ContextMenuStrip = mnu;
            if (e.Button == MouseButtons.Right)
            {
                row = e.RowIndex;
                col = e.ColumnIndex;
            }   
        }
#region datagrid_right_click_disable 
        private void mnufin_Click(object sender, EventArgs e)
        {
            OracleConnection conn = new OracleConnection("Data Source=QWE;Persist Security Info=True;User ID=QWE;Password=QWE;");
            OracleCommand sqlcom = new OracleCommand();
            sqlcom.Connection = conn;
            foreach (DataGridViewRow row in tasksgrid.SelectedRows)
            {
                    string value = Convert.ToString(row.Cells[0].Value);
                    try
                    {
                    conn.Open();
                    string temp = "UPDATE TASKS SET ENABLED = 0, DISABLESYSDATE = SYSDATE WHERE TASKID =TRIM('" + Convert.ToInt32(value) + "')";
                    sqlcom.CommandText = temp;
                    sqlcom.ExecuteNonQuery();
                    conn.Close();
                    }
                     catch (Exception ex)
                    {
                    MessageBox.Show(ex.Message);
                    } 
            }
            MessageBox.Show("Done!");
            if (Variables.flag == 1)
            {
                activetasksbn.PerformClick();
            }
            else if (Variables.flag == 2)
            {
                taskdataloadbn.PerformClick();
            }
            else if (Variables.flag == 3)
            {
                taskfinishbn.PerformClick();
            }
            else
            {
                MessageBox.Show("Could Not Load Data Correctly!");
            }
        }
        #endregion

#region datagrid_right_click_enable           
        private void mnuena_Click(object sender, EventArgs e)
        {
            OracleConnection conn = new OracleConnection("Data Source=QWE;Persist Security Info=True;User ID=QWE;Password=QWE;");
            OracleCommand sqlcom = new OracleCommand();
            sqlcom.Connection = conn;
            foreach (DataGridViewRow row in tasksgrid.SelectedRows)
            {
                string value = Convert.ToString(row.Cells[0].Value);
                try
                {
                    conn.Open();
                    string temp = "UPDATE TASKS SET ENABLED = 1, ENABLESYSDATE = SYSDATE WHERE TASKID =TRIM('" + Convert.ToInt32(value) + "')";
                    sqlcom.CommandText = temp;
                    sqlcom.ExecuteNonQuery();
                    conn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            MessageBox.Show("Done!");
            if (Variables.flag == 1)
            {
                activetasksbn.PerformClick();
            }
            else if (Variables.flag == 2)
            {
                taskdataloadbn.PerformClick();
            }
            else if (Variables.flag == 3)
            {
                taskfinishbn.PerformClick();
            }
            else
            {
                MessageBox.Show("Could Not Load Data Correctly!");
            }
        }
#endregion

    }
}

