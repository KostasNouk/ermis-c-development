﻿using System;
using System.Data;
using System.Text;
using System.Windows.Forms;
using Oracle.DataAccess.Client;
using System.Security.Cryptography;
using System.Text.RegularExpressions;

namespace Task_Scheduler_0._1_Beta
{
    public partial class Add_New_User : Form
    {
        public Add_New_User()
        {
            InitializeComponent();
        }
        private void newuserbn_Click(object sender, EventArgs e)
        {
            Variables.newnameboxtext = namebox.Text;
            Variables.newusernameboxtext = newusernamebox.Text;
            OracleConnection conn = new OracleConnection("Data Source=QWE;Persist Security Info=True;User ID=QWE;Password=QWE;");
            conn.Open();
            OracleCommand parsechoice = new OracleCommand();
            parsechoice.Connection = conn;
            string choiceparser = "SELECT GROUPID, DEPARTMENT FROM GROUPS";
            parsechoice.CommandText = choiceparser;
            string dbdata = parsechoice.ExecuteScalar().ToString();
            OracleDataReader reader = parsechoice.ExecuteReader();

            string selected = this.departmentchoice.GetItemText(this.departmentchoice.SelectedItem);
            //Fernw to department pou dialekse o xrhsths kai tsekarw an yparxei mesa sth vash. An yparxei ginetai dekth h kataxwrhsh tou neou user.
            while (reader.Read())
            {
                if (selected == Convert.ToString(reader[1]))
                {
                    Variables.departmentstring = Convert.ToString(reader[0]);
                }
            }
            conn.Close();
            accesslevelcheck();
            if (((Variables.accesslevel == 1) || (Variables.accesslevel == 2) )&& (Variables.departmentstring != ""))
            {
                executeinsert();
            }
            else if ((Variables.accesslevel == 3) && (Variables.departmentstring != ""))
            {
                Variables.createnewusername = newusernamebox.Text;
                Variables.global_pwd = pwdbox.Text;
                ERMIS_TS_0._5_Beta.departmentrights dep = new ERMIS_TS_0._5_Beta.departmentrights();
                dep.Show();
            }
            else
            {
                MessageBox.Show("You must choose department category and access level to continue.");
                clearvars();
            }
            clearvars();           
        }
#region accesslevelcheck
        public void accesslevelcheck()
        {
            if ((admin.Checked) && (!user.Checked) && (!subox.Checked))
            {
                Variables.accesslevel = 1;
            }
            else if ((!admin.Checked) && (!subox.Checked) && (user.Checked))
            {
                Variables.accesslevel = 2;
            }
            else if ((!admin.Checked) && (!user.Checked) && (subox.Checked))
            {
                Variables.accesslevel = 3;
            }
            else if ((!admin.Checked) && (!user.Checked) && (!subox.Checked))
            {
                MessageBox.Show("You have to choose an access level.");
            }
        }
#endregion
#region executeinsert 
        public void executeinsert()
        {
            Regex re = new Regex(@"^[0-9a-zA-Z ; @ < ) > . ( \s ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩαβγδεζηθικλμνξοπρστυφχψωάέήίόύς]+$");
            if (!re.IsMatch(namebox.Text) || !re.IsMatch(newusernamebox.Text) || !re.IsMatch(pwdbox.Text))
            {
            MessageBox.Show("Invalid Character Input!");
            }
            else
            {
            UTF8Encoding ue = new UTF8Encoding();
            byte[] hashValue;
            byte[] message = ue.GetBytes(pwdbox.Text);
            SHA256Managed hashString = new SHA256Managed();
            string hex = "";
            hashValue = hashString.ComputeHash(message);
            foreach (byte x in hashValue)
            {
                hex += String.Format("{0:x2}", x);
            }
            Variables.encrypted_pwd = hex;
            try
            {                 
                OracleConnection conn = new OracleConnection("Data Source=QWE;Persist Security Info=True;User ID=QWE;Password=QWE;");
                OracleCommand sqlcom = new OracleCommand();
                sqlcom.Connection = conn;
                string temp = "INSERT INTO USERS (USERID,USERNAME,NAME,PASSWORD, GROUPID, ACCESSLEVEL) VALUES((SELECT NVL(MAX(USERID),0)+1 FROM USERS),'" + this.newusernamebox.Text + "','" + this.namebox.Text + "','" + Variables.encrypted_pwd + "', '"+Variables.departmentstring+"', '"+Variables.accesslevel+"')";
                sqlcom.CommandText = temp;
                conn.Open();
                sqlcom.ExecuteNonQuery();
                conn.Close();
                MessageBox.Show("Done!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            }
        }
        #endregion
#region populatecombobox
        private void load_data_to_departmentchoice()
        {
            OracleConnection conn = new OracleConnection("Data Source=QWE;Persist Security Info=True;User ID=QWE;Password=QWE;");
            conn.Open();
            OracleDataAdapter ord = new OracleDataAdapter("SELECT GROUPS.DEPARTMENT FROM GROUPS;", conn);
            DataTable dt = new DataTable();
            ord.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; ++i)
            {
                departmentchoice.Items.Add(dt.Rows[i]["DEPARTMENT"]);
            }
            conn.Close();
        }

        private void Add_New_User_Load(object sender, EventArgs e)
        {
            load_data_to_departmentchoice();
        }
#endregion
#region init_vars
        public void clearvars()
        {
            Variables.accesslevel = 0;
            namebox.Text = "";
            newusernamebox.Text = "";
            pwdbox.Text = "";
        }
        #endregion
    }
}
