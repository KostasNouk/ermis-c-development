﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using Oracle.DataAccess.Client;

namespace Task_Scheduler_0._1_Beta
{
    public partial class Add_New_Dept : Form
    {
        public Add_New_Dept()
        {
            InitializeComponent();
        }

        private void newdeptbn_Click(object sender, EventArgs e)
        {
            Regex re = new Regex(@"^[0-9a-zA-Z ; @ < ) > . ( \s ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩαβγδεζηθικλμνξοπρστυφχψωάέήίόύςABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]+$");
            if (!re.IsMatch(deptnamebox.Text))
            {
                MessageBox.Show("Invalid Character Input!");
            }
            else
            {
                try
                {
                    OracleConnection conn = new OracleConnection("Data Source=QWE;Persist Security Info=True;User ID=QWE;Password=QWE;");
                    conn.Open();
                    OracleCommand sqlcom = new OracleCommand();
                    sqlcom.Connection = conn;
                    string temp = "INSERT INTO GROUPS (GROUPID,DEPARTMENT) VALUES((SELECT NVL(MAX(GROUPID),0)+1 FROM GROUPS),'"+this.deptnamebox.Text+"')";
                    sqlcom.CommandText = temp;
                    sqlcom.ExecuteNonQuery();
                    conn.Close();
                    MessageBox.Show("Done!");
                    deptnamebox.Text = "";
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }
    }
}
