﻿namespace Task_Scheduler_0._1_Beta
{
    partial class Add_New_Task
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Add_New_Task));
            this.addtaskbtn = new System.Windows.Forms.Button();
            this.newtasknamebox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.newtasknotesbox = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.startdate = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.choiceinterval = new System.Windows.Forms.ComboBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.departmentbox = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // addtaskbtn
            // 
            this.addtaskbtn.BackColor = System.Drawing.Color.Firebrick;
            this.addtaskbtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.addtaskbtn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.addtaskbtn.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.addtaskbtn.ForeColor = System.Drawing.Color.White;
            this.addtaskbtn.Location = new System.Drawing.Point(18, 366);
            this.addtaskbtn.Name = "addtaskbtn";
            this.addtaskbtn.Size = new System.Drawing.Size(141, 32);
            this.addtaskbtn.TabIndex = 0;
            this.addtaskbtn.Text = "Add Task";
            this.addtaskbtn.UseVisualStyleBackColor = false;
            this.addtaskbtn.Click += new System.EventHandler(this.button1_Click);
            // 
            // newtasknamebox
            // 
            this.newtasknamebox.Location = new System.Drawing.Point(18, 37);
            this.newtasknamebox.Multiline = true;
            this.newtasknamebox.Name = "newtasknamebox";
            this.newtasknamebox.Size = new System.Drawing.Size(253, 20);
            this.newtasknamebox.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label1.Location = new System.Drawing.Point(15, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 18);
            this.label1.TabIndex = 2;
            this.label1.Text = "New Task Name:";
            // 
            // newtasknotesbox
            // 
            this.newtasknotesbox.Location = new System.Drawing.Point(18, 90);
            this.newtasknotesbox.Multiline = true;
            this.newtasknotesbox.Name = "newtasknotesbox";
            this.newtasknotesbox.Size = new System.Drawing.Size(253, 104);
            this.newtasknotesbox.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label4.Location = new System.Drawing.Point(15, 69);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(110, 18);
            this.label4.TabIndex = 8;
            this.label4.Text = "New Task Notes:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label2.Location = new System.Drawing.Point(15, 206);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 18);
            this.label2.TabIndex = 10;
            this.label2.Text = "Task Start Date:";
            // 
            // startdate
            // 
            this.startdate.CustomFormat = "dd-MM-yyyy HH:mm:ss";
            this.startdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.startdate.Location = new System.Drawing.Point(18, 227);
            this.startdate.Name = "startdate";
            this.startdate.Size = new System.Drawing.Size(253, 21);
            this.startdate.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label3.Location = new System.Drawing.Point(15, 256);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(92, 18);
            this.label3.TabIndex = 12;
            this.label3.Text = "Set Schedule:";
            // 
            // choiceinterval
            // 
            this.choiceinterval.BackColor = System.Drawing.Color.White;
            this.choiceinterval.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.choiceinterval.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.choiceinterval.FormattingEnabled = true;
            this.choiceinterval.Items.AddRange(new object[] {
            "Every Day",
            "Every 2 Days",
            "Every 3 Days",
            "Every 4 Days",
            "Every 5 Days",
            "Every Week",
            "Every 2 Weeks",
            "Every Month",
            "Every Minute (Tests Only)"});
            this.choiceinterval.Location = new System.Drawing.Point(18, 277);
            this.choiceinterval.Name = "choiceinterval";
            this.choiceinterval.Size = new System.Drawing.Size(253, 21);
            this.choiceinterval.TabIndex = 13;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox6.BackColor = System.Drawing.Color.Firebrick;
            this.pictureBox6.Location = new System.Drawing.Point(-10, -8);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(19, 437);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 20;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.BackColor = System.Drawing.Color.Firebrick;
            this.pictureBox1.Location = new System.Drawing.Point(307, -8);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(10, 437);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 21;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox5.BackColor = System.Drawing.Color.Firebrick;
            this.pictureBox5.Location = new System.Drawing.Point(0, 413);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(317, 13);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 22;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox2.BackColor = System.Drawing.Color.Firebrick;
            this.pictureBox2.Location = new System.Drawing.Point(0, -8);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(317, 15);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 23;
            this.pictureBox2.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label5.Location = new System.Drawing.Point(15, 306);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(87, 18);
            this.label5.TabIndex = 24;
            this.label5.Text = "Department:";
            // 
            // departmentbox
            // 
            this.departmentbox.BackColor = System.Drawing.Color.White;
            this.departmentbox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.departmentbox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.departmentbox.ForeColor = System.Drawing.SystemColors.WindowText;
            this.departmentbox.FormattingEnabled = true;
            this.departmentbox.Location = new System.Drawing.Point(18, 327);
            this.departmentbox.Name = "departmentbox";
            this.departmentbox.Size = new System.Drawing.Size(253, 21);
            this.departmentbox.TabIndex = 25;
            // 
            // Add_New_Task
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(316, 421);
            this.Controls.Add(this.departmentbox);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.choiceinterval);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.startdate);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.newtasknotesbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.newtasknamebox);
            this.Controls.Add(this.addtaskbtn);
            this.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(332, 393);
            this.Name = "Add_New_Task";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Add New Task";
            this.Load += new System.EventHandler(this.Add_New_Task_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button addtaskbtn;
        private System.Windows.Forms.TextBox newtasknamebox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox newtasknotesbox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker startdate;
        private System.Windows.Forms.Label label3;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.ComboBox choiceinterval;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox departmentbox;
    }
}