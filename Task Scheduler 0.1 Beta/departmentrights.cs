﻿using System;
using System.Data;
using System.Windows.Forms;
using Oracle.DataAccess.Client;
using System.Security.Cryptography;
using System.Text;

namespace ERMIS_TS_0._5_Beta
{
    public partial class departmentrights : Form
    {
        public departmentrights()
        {
            InitializeComponent();           
        }

        DataSet ds = new DataSet();
        OracleDataAdapter OracleAdapter = null;

        private void departmentrights_Load(object sender, EventArgs e)
        {
            Populate_DataSet();
            FillCheckListBox();
        }

        private void newuserbn_Click(object sender, EventArgs e)
        {
            string s_Value = "";
            OracleConnection conn = new OracleConnection("Data Source=QWE;Persist Security Info=True;User ID=QWE;Password=QWE;");
            conn.Open();
            OracleCommand cmd = new OracleCommand();
            cmd.Connection = conn;
           
            try
            {
            encrypt();
            string temp = "INSERT INTO USERS (USERID,USERNAME,NAME,PASSWORD, GROUPID, ACCESSLEVEL) VALUES((SELECT NVL(MAX(USERID),0)+1 FROM USERS),'" + Task_Scheduler_0._1_Beta.Variables.newusernameboxtext + "','" + Task_Scheduler_0._1_Beta.Variables.newnameboxtext + "','" + Task_Scheduler_0._1_Beta.Variables.encrypted_pwd + "', '" + Task_Scheduler_0._1_Beta.Variables.departmentstring + "', 3)";
            cmd.CommandText = temp;               
            cmd.ExecuteNonQuery();
            MessageBox.Show("Done!");
            }
            catch (Exception ex)
            {
            MessageBox.Show(ex.Message);
            }

            //---------------------------------------------------------------------------------------------------------------------
            //Πρέπει να κάνω πρώτα το insert στον πίνακα Users έτσι ώστε να τραβήξω το id του και να το περνάω μέσα στον Userrights
            OracleCommand parseuserid = new OracleCommand();
            parseuserid.Connection = conn;
            string usridparser = "SELECT * FROM USERS WHERE TRIM(USERNAME) = TRIM('" +Task_Scheduler_0._1_Beta.Variables.newusernameboxtext+ "')";
            parseuserid.CommandText = usridparser;
            string newuserid = parseuserid.ExecuteScalar().ToString();
            OracleDataReader getnewuserid = parseuserid.ExecuteReader();

            while (getnewuserid.Read())
            {
                string meinID = Convert.ToString(getnewuserid[0]);
                Task_Scheduler_0._1_Beta.Variables.user_id = meinID; ;
            }
            //----------------------------------------------------------------------------------------------------------------------
            if (userrightsbx.Items.Count > 0)
            {
                for (int i = 0; i <= userrightsbx.CheckedItems.Count - 1; i++)
                {
                    s_Value = userrightsbx.CheckedItems[i].ToString();
                    OracleCommand parsedept = new OracleCommand();
                    parsedept.Connection = conn;
                    string deptparser = "SELECT * FROM GROUPS WHERE TRIM(DEPARTMENT) = TRIM('" + s_Value + "')";
                    parsedept.CommandText = deptparser;
                    string mydept = parsedept.ExecuteScalar().ToString();
                    OracleDataReader reader = parsedept.ExecuteReader();
                    while (reader.Read())
                    {
                        string meindept = Convert.ToString(reader[0]);
                        cmd.CommandText = "INSERT INTO USERRIGHTS(ID, USERID, GROUPID) VALUES((SELECT NVL(MAX(ID), 0) + 1 FROM USERRIGHTS), '" + Task_Scheduler_0._1_Beta.Variables.user_id + "', '" + meindept + "')";
                        cmd.ExecuteNonQuery();
                    }   
                }
            }
            conn.Close();
        }

        private void Populate_DataSet()
        {
            string s_ConnString = "Data Source=QWE;Persist Security Info=True;User ID=QWE;Password=QWE;";
            using (OracleConnection con = new OracleConnection(s_ConnString))
            {
                string cmd = "SELECT * FROM GROUPS";
                OracleAdapter = new OracleDataAdapter(cmd, con);  
                OracleAdapter.Fill(ds,"GROUPID");
            }
        }
     
        private void FillCheckListBox()
        {
            DataRow row = null;
            int iRowCnt = 0;
            userrightsbx.Items.Clear();

            foreach (DataRow row_1 in ds.Tables["GROUPID"].Rows)
            {
                row = row_1;
                userrightsbx.Items.Add(ds.Tables["GROUPID"].Rows[iRowCnt][1]);
                iRowCnt = iRowCnt + 1;
            }
        }
            
#region prepareencryption
public void encrypt()
   {          
     UTF8Encoding ue = new UTF8Encoding();
     byte[] hashValue;
     byte[] message = ue.GetBytes(Task_Scheduler_0._1_Beta.Variables.global_pwd);
     SHA256Managed hashString = new SHA256Managed();
     string hex = "";
     hashValue = hashString.ComputeHash(message);
     foreach (byte x in hashValue)
       {
           hex += String.Format("{0:x2}", x);
       }
     Task_Scheduler_0._1_Beta.Variables.encrypted_pwd = hex;
   }
#endregion 
    }
}
