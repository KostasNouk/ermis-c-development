﻿namespace Task_Scheduler_0._1_Beta
{
    partial class Show_tasks
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Show_tasks));
            this.showtasks = new System.Windows.Forms.Label();
            this.taskexitbn = new System.Windows.Forms.Button();
            this.taskfinishbn = new System.Windows.Forms.Button();
            this.activetasksbn = new System.Windows.Forms.Button();
            this.taskdataloadbn = new System.Windows.Forms.Button();
            this.tasksgrid = new System.Windows.Forms.DataGridView();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.tasksgrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // showtasks
            // 
            this.showtasks.AutoSize = true;
            this.showtasks.Font = new System.Drawing.Font("Calibri", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.showtasks.Location = new System.Drawing.Point(176, 24);
            this.showtasks.Name = "showtasks";
            this.showtasks.Size = new System.Drawing.Size(82, 26);
            this.showtasks.TabIndex = 16;
            this.showtasks.Text = "Task List";
            // 
            // taskexitbn
            // 
            this.taskexitbn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.taskexitbn.BackColor = System.Drawing.Color.Firebrick;
            this.taskexitbn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.taskexitbn.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.taskexitbn.ForeColor = System.Drawing.Color.White;
            this.taskexitbn.Location = new System.Drawing.Point(582, 421);
            this.taskexitbn.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.taskexitbn.Name = "taskexitbn";
            this.taskexitbn.Size = new System.Drawing.Size(137, 26);
            this.taskexitbn.TabIndex = 22;
            this.taskexitbn.Text = "Exit";
            this.taskexitbn.UseVisualStyleBackColor = false;
            this.taskexitbn.Click += new System.EventHandler(this.taskexitbn_Click);
            // 
            // taskfinishbn
            // 
            this.taskfinishbn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.taskfinishbn.BackColor = System.Drawing.Color.Firebrick;
            this.taskfinishbn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.taskfinishbn.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.taskfinishbn.ForeColor = System.Drawing.Color.White;
            this.taskfinishbn.Location = new System.Drawing.Point(383, 421);
            this.taskfinishbn.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.taskfinishbn.Name = "taskfinishbn";
            this.taskfinishbn.Size = new System.Drawing.Size(161, 26);
            this.taskfinishbn.TabIndex = 21;
            this.taskfinishbn.Text = "Show Disabled Tasks";
            this.taskfinishbn.UseVisualStyleBackColor = false;
            this.taskfinishbn.Click += new System.EventHandler(this.taskfinishbn_Click);
            // 
            // activetasksbn
            // 
            this.activetasksbn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.activetasksbn.BackColor = System.Drawing.Color.Firebrick;
            this.activetasksbn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.activetasksbn.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.activetasksbn.ForeColor = System.Drawing.Color.White;
            this.activetasksbn.Location = new System.Drawing.Point(30, 421);
            this.activetasksbn.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.activetasksbn.Name = "activetasksbn";
            this.activetasksbn.Size = new System.Drawing.Size(137, 26);
            this.activetasksbn.TabIndex = 20;
            this.activetasksbn.Text = "Show Active Tasks";
            this.activetasksbn.UseVisualStyleBackColor = false;
            this.activetasksbn.Click += new System.EventHandler(this.activetasksbn_Click);
            // 
            // taskdataloadbn
            // 
            this.taskdataloadbn.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.taskdataloadbn.BackColor = System.Drawing.Color.Firebrick;
            this.taskdataloadbn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.taskdataloadbn.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.taskdataloadbn.ForeColor = System.Drawing.Color.White;
            this.taskdataloadbn.Location = new System.Drawing.Point(207, 421);
            this.taskdataloadbn.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.taskdataloadbn.Name = "taskdataloadbn";
            this.taskdataloadbn.Size = new System.Drawing.Size(137, 26);
            this.taskdataloadbn.TabIndex = 18;
            this.taskdataloadbn.Text = "Load All Results";
            this.taskdataloadbn.UseVisualStyleBackColor = false;
            this.taskdataloadbn.Click += new System.EventHandler(this.taskdataloadbn_Click);
            // 
            // tasksgrid
            // 
            this.tasksgrid.AllowUserToAddRows = false;
            this.tasksgrid.AllowUserToDeleteRows = false;
            this.tasksgrid.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tasksgrid.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.tasksgrid.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tasksgrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.tasksgrid.GridColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.tasksgrid.Location = new System.Drawing.Point(36, 65);
            this.tasksgrid.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.tasksgrid.Name = "tasksgrid";
            this.tasksgrid.ReadOnly = true;
            this.tasksgrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.tasksgrid.Size = new System.Drawing.Size(683, 338);
            this.tasksgrid.TabIndex = 17;
            this.tasksgrid.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.tasksgrid_CellMouseDown);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.BackColor = System.Drawing.Color.Firebrick;
            this.pictureBox3.Location = new System.Drawing.Point(0, 56);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(747, 3);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox3.TabIndex = 15;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::ERMIS_TS_0._5_Beta.Properties.Resources.DIASlogo_CMYK_hr2;
            this.pictureBox2.Location = new System.Drawing.Point(12, 12);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(140, 47);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 14;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox6
            // 
            this.pictureBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox6.BackColor = System.Drawing.Color.Firebrick;
            this.pictureBox6.Location = new System.Drawing.Point(737, 0);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(10, 477);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox6.TabIndex = 13;
            this.pictureBox6.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox4.BackColor = System.Drawing.Color.Firebrick;
            this.pictureBox4.Location = new System.Drawing.Point(0, 467);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(747, 20);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox4.TabIndex = 12;
            this.pictureBox4.TabStop = false;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.pictureBox7.BackColor = System.Drawing.Color.Firebrick;
            this.pictureBox7.Location = new System.Drawing.Point(0, 0);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(10, 477);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 11;
            this.pictureBox7.TabStop = false;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox5.BackColor = System.Drawing.Color.Firebrick;
            this.pictureBox5.Location = new System.Drawing.Point(0, 0);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(747, 10);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox5.TabIndex = 9;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.Image = global::ERMIS_TS_0._5_Beta.Properties.Resources.clock_icon;
            this.pictureBox1.Location = new System.Drawing.Point(684, 12);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(47, 41);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 35;
            this.pictureBox1.TabStop = false;
            // 
            // Show_tasks
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(747, 479);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.taskexitbn);
            this.Controls.Add(this.taskfinishbn);
            this.Controls.Add(this.activetasksbn);
            this.Controls.Add(this.taskdataloadbn);
            this.Controls.Add(this.tasksgrid);
            this.Controls.Add(this.showtasks);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox6);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox7);
            this.Controls.Add(this.pictureBox5);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(763, 517);
            this.Name = "Show_tasks";
            this.Text = "Show Tasks";
            ((System.ComponentModel.ISupportInitialize)(this.tasksgrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label showtasks;
        private System.Windows.Forms.Button taskexitbn;
        private System.Windows.Forms.Button taskfinishbn;
        private System.Windows.Forms.Button activetasksbn;
        private System.Windows.Forms.Button taskdataloadbn;
        private System.Windows.Forms.DataGridView tasksgrid;
       // private System.Windows.Forms.ContextMenuStrip mnu;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}