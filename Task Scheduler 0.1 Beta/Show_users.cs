﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace Task_Scheduler_0._1_Beta
{
    public partial class Show_users : Form
    {
        public Show_users()
        {
            InitializeComponent();
        }

        private void usersdataloadbn_Click(object sender, EventArgs e)
        {
            try
            {
                OracleConnection conn = new OracleConnection("Data Source=QWE;Persist Security Info=True;User ID=QWE;Password=QWE;");
                OracleCommand sqlcom = new OracleCommand();
                sqlcom.Connection = conn;
                string loadselect = "SELECT * FROM QWE.USERS ORDER BY USERID ASC";
                sqlcom.CommandText = loadselect;
                conn.Open();
                OracleDataAdapter odb = new OracleDataAdapter();
                odb.SelectCommand = sqlcom;
                DataTable dbdataset = new DataTable();
                odb.Fill(dbdataset);
                BindingSource bsource = new BindingSource();
                bsource.DataSource = dbdataset;
                usersgrid.DataSource = bsource;
                odb.Update(dbdataset);
                conn.Close();
                //MessageBox.Show("Done!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void usersexitbn_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
