﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ERMIS_TS_0._5_Beta
{
    public partial class sumainform : Form
    {
        public sumainform()
        {
            InitializeComponent();
        }

        private void sumainform_Load(object sender, EventArgs e)
        {
            loggedusername.Text = "Welcome to Ermis, " + Task_Scheduler_0._1_Beta.Variables.usrid + "!";
        }

        private void showTasksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Task_Scheduler_0._1_Beta.Show_tasks s = new Task_Scheduler_0._1_Beta.Show_tasks();
            s.Show();
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            Task_Scheduler_0._1_Beta.Add_New_Task a = new Task_Scheduler_0._1_Beta.Add_New_Task();
            a.Show();
        }

        private void sumainform_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to exit ERMIS TS?", "Exit", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                Environment.Exit(0);
            }
        }

        private void schedulerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Task_Scheduler_0._1_Beta.Scheduler sc = new Task_Scheduler_0._1_Beta.Scheduler();
            sc.Show();
        }
    }
}
