﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Oracle.DataAccess.Client;

namespace Task_Scheduler_0._1_Beta
{
    public partial class Task_Scheduler : Form
    {
        public Task_Scheduler()
        {
            InitializeComponent();
        }

        #region show_adduser_addtask_adddepartment
        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            Add_New_Task x = new Add_New_Task();
            x.Show();
        }
        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            Add_New_User usr = new Add_New_User();
            usr.Show();
        }

        private void addNewDepartmentToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Add_New_Dept newdept = new Add_New_Dept();
            newdept.Show();
        }
        #endregion
       
        #region show_tasks_users_departments_forms
        private void showTasksToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Show_tasks st = new Show_tasks();
            st.Show();
        }
        private void showUsersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Show_users su = new Show_users();
            su.Show();
        }

        private void showdepartments_Click_1(object sender, EventArgs e)
        {
            Departments de = new Departments();
            de.Show();
        }
        #endregion

        #region scheduler
        private void schedulerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Scheduler sc = new Scheduler();
            sc.Show();
        }
        #endregion

        void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to exit ERMIS TS?", "Exit", MessageBoxButtons.OKCancel) == DialogResult.OK)
            {
                Environment.Exit(0);
            }       
        }
        private void Task_Scheduler_Load(object sender, EventArgs e)
        {
            loggedusername.Text = "Welcome to Ermis, "+Variables.usrid+"!";
        }
    }
}
