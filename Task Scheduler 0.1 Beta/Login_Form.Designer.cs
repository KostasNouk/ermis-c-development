﻿namespace Task_Scheduler_0._1_Beta
{
    partial class Login_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login_Form));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.usrnmtxtbx = new System.Windows.Forms.TextBox();
            this.usrpwdtxtbx = new System.Windows.Forms.TextBox();
            this.cancelbn = new System.Windows.Forms.Button();
            this.loginbn = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label1.Location = new System.Drawing.Point(152, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(76, 18);
            this.label1.TabIndex = 0;
            this.label1.Text = "Username:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.label2.Location = new System.Drawing.Point(152, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "Password:";
            // 
            // usrnmtxtbx
            // 
            this.usrnmtxtbx.Location = new System.Drawing.Point(153, 30);
            this.usrnmtxtbx.Name = "usrnmtxtbx";
            this.usrnmtxtbx.Size = new System.Drawing.Size(180, 21);
            this.usrnmtxtbx.TabIndex = 3;
            // 
            // usrpwdtxtbx
            // 
            this.usrpwdtxtbx.Location = new System.Drawing.Point(152, 70);
            this.usrpwdtxtbx.Name = "usrpwdtxtbx";
            this.usrpwdtxtbx.PasswordChar = '*';
            this.usrpwdtxtbx.Size = new System.Drawing.Size(181, 21);
            this.usrpwdtxtbx.TabIndex = 4;
            // 
            // cancelbn
            // 
            this.cancelbn.BackColor = System.Drawing.Color.Firebrick;
            this.cancelbn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cancelbn.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.cancelbn.ForeColor = System.Drawing.Color.White;
            this.cancelbn.Location = new System.Drawing.Point(12, 117);
            this.cancelbn.Name = "cancelbn";
            this.cancelbn.Size = new System.Drawing.Size(134, 29);
            this.cancelbn.TabIndex = 5;
            this.cancelbn.Text = "Cancel";
            this.cancelbn.UseVisualStyleBackColor = false;
            this.cancelbn.Click += new System.EventHandler(this.cancelbn_Click);
            // 
            // loginbn
            // 
            this.loginbn.BackColor = System.Drawing.Color.Firebrick;
            this.loginbn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.loginbn.Font = new System.Drawing.Font("Calibri", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.loginbn.ForeColor = System.Drawing.Color.White;
            this.loginbn.Location = new System.Drawing.Point(152, 117);
            this.loginbn.Name = "loginbn";
            this.loginbn.Size = new System.Drawing.Size(181, 29);
            this.loginbn.TabIndex = 6;
            this.loginbn.Text = "Login";
            this.loginbn.UseVisualStyleBackColor = false;
            this.loginbn.Click += new System.EventHandler(this.loginbn_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::ERMIS_TS_0._5_Beta.Properties.Resources.DIASlogo_CMYK_hr2;
            this.pictureBox1.Location = new System.Drawing.Point(3, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(153, 106);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // Login_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(345, 158);
            this.Controls.Add(this.loginbn);
            this.Controls.Add(this.cancelbn);
            this.Controls.Add(this.usrpwdtxtbx);
            this.Controls.Add(this.usrnmtxtbx);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(161)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(361, 196);
            this.MinimumSize = new System.Drawing.Size(361, 196);
            this.Name = "Login_Form";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Log In to ERMIS TS";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox usrnmtxtbx;
        private System.Windows.Forms.TextBox usrpwdtxtbx;
        private System.Windows.Forms.Button cancelbn;
        private System.Windows.Forms.Button loginbn;
    }
}